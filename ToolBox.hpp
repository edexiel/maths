#ifndef __TOOLBOX_HPP__
#define __TOOLBOX_HPP__

#include <math.h>
namespace Core::Maths
{

    class ToolBox
    {
    private:
        static bool validInterval(float n);

    public:
        static float solve(float a, float b, float c, float *result);
    };

    static bool validInterval(float n)
    {
        return n > 0 && n <= 1;
    }

    // Solves a quadratic equation, returns true if there is an answer or false;
    // if there a two results, return the lowest [0;1] result
    inline bool solve(float a, float b, float c, float *result)
    {
        float delta{a * a - 4 * a * c};

        if (delta < 0) // no solution
        {
            return false;
        }
        else if (delta <= __FLT_EPSILON__) //equals 0, 1 solution
        {
            float res{-b / 2 * a};
            *result = validInterval(res) ? res : 0;
            return validInterval(res);
        }
        else // >0, 2 solutions
        {
            float sqrtdelta = sqrtf(delta);

            float t1{(-b - sqrtdelta) / (2 * a)};
            float t2{(-b + sqrtdelta) / (2 * a)};

            bool bt1{validInterval(t1)};
            bool bt2{validInterval(t2)};

            if (bt1 && bt2)
            {
                *result = t1 < t2 ? t1 : t2;
                return true;
            }

            if (bt1 || bt2)
            {
                *result = bt1 ? t1 : t2;
                return bt1 || bt2;
            }
        }
    }
} // namespace Core::Maths

#endif // __TOOLBOX_HPP__
