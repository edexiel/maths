#ifndef __QUATERNION_HPP__
#define __QUATERNION_HPP__

#include <math.h>
#include "Vec3.hpp"
#include "Mat4.hpp"

namespace Core::Maths
{

    class Quaternion
    {

    public:
        union
        {
            struct
            {
                float x, y, z, w;
            };
            float e[4];
        };

        Quaternion() = default;
        Quaternion(float _x, float _y, float _z, float _w);
        Quaternion(float roll, float pitch, float yaw);
        ~Quaternion() = default;

        float length() const;

        Vec3 ToEuler() const;

        Quaternion normalize();
        Quaternion &invert();
        Quaternion identity() const;
        Quaternion isPure() const;

        Vec3 rotate(const Vec3 &vector) const;

        static Quaternion lerp(const Quaternion &q1, const Quaternion &q2, float value);
        static Quaternion slerp(const Quaternion &q1, const Quaternion &q2, float value);
        static Quaternion nlerp(const Quaternion &q1, const Quaternion &q2, float value);

        float dot(const Quaternion &quaternion);

        Mat4 toMat4() const;

        Quaternion operator+(Quaternion &other);
        Quaternion operator+(float value);
        Quaternion operator-(Quaternion &other);
        Quaternion operator-(float value);
        Quaternion operator*(Quaternion &other);
        Quaternion operator*(float scale);
    };

    Quaternion::Quaternion(float _x, float _y, float _z, float _w) : x{_x}, y{_y}, z{_z}, w{_w} {}

    Quaternion::Quaternion(float roll, float pitch, float yaw)
    {
    }

    inline float Quaternion::length() const
    {
        return sqrtf(x * x + y * y + z * z + w * w);
    }

    inline Vec3 Quaternion::ToEuler() const
    {
    }

    inline Quaternion Quaternion::normalize()
    {
        float len = length();
        //todo : test if len =0.f
        return (*this)*(1.f/len);
    }
    inline Quaternion &Quaternion::invert()
    {
    }
    inline Quaternion Quaternion::identity() const
    {
        return Quaternion{0.f,0.f,0.f,1.f};
    }
    inline Quaternion Quaternion::isPure() const
    {

    }
    inline Quaternion Quaternion::lerp(const Quaternion &q1, const Quaternion &q2, float value)
    {

    }
    inline Quaternion Quaternion::slerp(const Quaternion &q1, const Quaternion &q2, float value)
    {

    }
    inline Quaternion Quaternion::nlerp(const Quaternion &q1, const Quaternion &q2, float value)
    {

    }
    inline Quaternion Quaternion::operator+(Quaternion &other)
    {
        return Quaternion{x + other.x, y + other.y, z + other.z, w + other.z};
    }

    inline Quaternion Quaternion::operator+(float value)
    {
        return Quaternion{x + value, y + value, z + value, w + value};
    }

    inline Quaternion Quaternion::operator-(Quaternion &other)
    {
        return Quaternion{x - other.x, y - other.y, z - other.z, w - other.z};
    }
    inline Quaternion Quaternion::operator-(float value)
    {
        return Quaternion{x - value, y - value, z - value, w - value};
    }
    inline Quaternion Quaternion::operator*(Quaternion &other)
    {
    }
    inline Quaternion Quaternion::operator*(float scale)
    {
    }

} // namespace Core::Maths

#endif // __QUATERNION_HPP__
