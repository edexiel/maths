#ifndef __MATRIX_HPP__
#define __MATRIX_HPP__


#include <iostream>
#include <cstring>

namespace Core::Maths
{

class Matrix
{
private:
    unsigned int _line;
    unsigned int _column;

public:
    Matrix() = default;
    Matrix(unsigned int line, unsigned int colums);
    Matrix(const Matrix &);
    ~Matrix();

    static Matrix zero(unsigned int line, unsigned int column);
    static Matrix identity(unsigned int size);
    float determinant();

    float *matrix;

    unsigned int get_nb_line() const;
    unsigned int get_nb_col() const;

    bool is_square();
    bool is_orthogonal();

    float mat_minor(unsigned int line, unsigned int column);
    float cofactor(unsigned int i, unsigned int j);

    Matrix comatrix();
    Matrix adjugate();
    Matrix transpose();
    Matrix invert();

    bool operator==(const Matrix &other);
    Matrix operator+(const Matrix &other);
    Matrix operator-(const Matrix &other);
    Matrix operator*(const Matrix &other);
    Matrix operator*(const float f);
    float *operator[](unsigned int index) const;
};

inline Matrix::Matrix(unsigned int line, unsigned int column) : matrix{new float[line * column]{0}},
                                                                _line{line},
                                                                _column{column} {}

inline Matrix::Matrix(const Matrix &m)
{
    unsigned int d{m.get_nb_line() * m.get_nb_col()};

    matrix = new float[d];
    mempcpy(matrix, m.matrix, sizeof(float) * d);

    _line = m.get_nb_line();
    _column = m.get_nb_col();
}

inline Matrix::~Matrix()
{
    delete[] matrix;
}

inline Matrix Matrix::zero(unsigned int line, unsigned int column)
{
    Matrix r(line, column);
    r.matrix = new float[line * column]{0};
    return r;
}

inline Matrix Matrix::identity(unsigned int size)
{
    Matrix mat(size, size);

    unsigned int index{0};

    for (unsigned int i = 0; i < size * size; i++)
    {
        if (i == index)
        {
            mat.matrix[i] = 1;
            index += size + 1;
        }
        else
            mat.matrix[i] = 0;
    }
    return mat;
}

/**
 * @brief Return the determinant of 2, 3 and 4 dimension square matrix
 * 
 * @return float 
 */
inline float Matrix::determinant()
{
    if (get_nb_col() != get_nb_line())
        return 0;

    if (get_nb_line() == 1)
        return matrix[0];
    else if (get_nb_line() == 2)
        return matrix[0] * matrix[3] - matrix[1] * matrix[2];
    else if (get_nb_line() == 3)
        return matrix[0] * matrix[4] * matrix[8] + matrix[1] * matrix[5] * matrix[6] + matrix[2] * matrix[3] * matrix[7] -
               matrix[2] * matrix[4] * matrix[6] - matrix[0] * matrix[5] * matrix[7] - matrix[1] * matrix[3] * matrix[8];
    else if (get_nb_line() == 4)
        return matrix[0] * (matrix[5] * matrix[10] * matrix[15] + matrix[6] * matrix[11] * matrix[13] + matrix[7] * matrix[9] * matrix[14] -
                            matrix[7] * matrix[10] * matrix[13] - matrix[6] * matrix[9] * matrix[15] - matrix[5] * matrix[11] * matrix[14]) -
               matrix[1] * (matrix[4] * matrix[10] * matrix[15] + matrix[6] * matrix[11] * matrix[12] + matrix[7] * matrix[8] * matrix[14] -
                            matrix[7] * matrix[10] * matrix[12] - matrix[6] * matrix[8] * matrix[15] - matrix[4] * matrix[11] * matrix[14]) +
               matrix[0] * (matrix[4] * matrix[9] * matrix[15] + matrix[5] * matrix[11] * matrix[12] + matrix[7] * matrix[8] * matrix[13] -
                            matrix[7] * matrix[9] * matrix[12] - matrix[5] * matrix[8] * matrix[15] - matrix[4] * matrix[11] * matrix[13]) -
               matrix[0] * (matrix[4] * matrix[9] * matrix[14] + matrix[5] * matrix[10] * matrix[12] + matrix[6] * matrix[8] * matrix[13] -
                            matrix[6] * matrix[9] * matrix[12] - matrix[5] * matrix[8] * matrix[14] - matrix[4] * matrix[10] * matrix[13]);

    return 0;
}

inline unsigned int Matrix::get_nb_line() const
{
    return _line;
}

inline unsigned int Matrix::get_nb_col() const
{
    return _column;
}

inline bool Matrix::is_square()
{
    return _line == _column;
}

//faux
bool Matrix::is_orthogonal()
{
    return this->transpose() * *this == identity(_line);
}

inline float Matrix::mat_minor(unsigned int n, unsigned int m)
{
    if (!this->is_square())
        return 0;

    (void)n;
    (void)m;
}

inline float Matrix::cofactor(unsigned int n, unsigned int m)
{
    if ((n + m) % 2 == 0)
        return mat_minor(n, m);
    else
        return -mat_minor(n, m);
}

inline Matrix Matrix::comatrix()
{
    if (!this->is_square())
        return zero(_line, _column);

    Matrix result(_line, _column);

    for (size_t i = 0; i < _line; i++)
    {
        for (size_t j = 0; j < _column; j++)
        {
            result[i][j] = cofactor(i + 1, j + 1);
        }
    }
    return result;
}

inline Matrix Matrix::transpose()
{
    Matrix result{_line, _column};

    for (size_t i = 0; i < _line; i++)
    {
        for (size_t j = 0; i < _column; i++)
        {
            result[j][i] = *this[i][j];
        }
    }
    return result;
}

inline Matrix Matrix::adjugate()
{
    return comatrix().transpose();
}

inline Matrix Matrix::invert()
{
    if (!is_square())
        return zero(_line, _column);

    float inv_det = (float)1 / determinant();

    return this->adjugate() * inv_det;
}

inline float *Matrix::operator[](unsigned int index) const
{
    return &matrix[index * _line];
}

inline bool Matrix::operator==(const Matrix &other)
{
    if (_line != other._line || _column != other._column)
        return false;

    for (unsigned int i = 0; i < _line * _column; i++)
    {
        if (matrix[i] != other.matrix[i])
            return false;
    }

    return true;
}
inline Matrix Matrix::operator+(const Matrix &other)
{
    if (_line != other._line || _column != other._column)
        return *this;

    Matrix result{_line, _column};

    for (unsigned int i = 0; i < _line * _column; i++)
        result.matrix[i] = matrix[i] + other.matrix[i];

    return result;
}

inline Matrix Matrix::operator-(const Matrix &other)
{
    if (_line != other._line || _column != other._column)
        return *this;

    Matrix result{_line, _column};

    for (unsigned int i = 0; i < _line * _column; i++)
        result.matrix[i] = matrix[i] - other.matrix[i];

    return result;
}

inline Matrix Matrix::operator*(const Matrix &other)
{
    if (_line != other._column)
        return zero(_line, _column);

    Matrix res{_line, other._column};

    for (int c = 0; c < _line; ++c)
        for (int r = 0; r < other._column; ++r)
            for (int i = 0; i < _column; ++i)
                res[r][c] += (*this)[r][i] * other[i][c];

    return res;
}

inline Matrix Matrix::operator*(const float f)
{
    if (f == 0.f)
        return zero(_line, _column);

    Matrix result{_line, _column};

    for (unsigned int i = 0; i < _line * _column; i++)
        *result[i] = matrix[i] * f;

    return result;
}

inline std::ostream &operator<<(std::ostream &os, Matrix &mat)
{
    for (unsigned int i = 0; i < mat.get_nb_line(); i++)
    {
        for (unsigned int j = 0; j < mat.get_nb_col(); j++)
        {
            os << mat[i][j] << ", ";
        }
        os << std::endl;
    }
    return os;
}
} // namespace Core::Maths


#endif // __MATRIX_HPP__

