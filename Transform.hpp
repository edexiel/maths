#pragma once

#include "Vec4.hpp"
#include "Vec3.hpp"

namespace Physics
{
struct Transform
{
    Core::Maths::Vec4 position = {0.f, 0.f, 0.f, 1.0f};
    Core::Maths::Vec3 rotation = {0.f, 0.f, 0.f};
    Core::Maths::Vec3 scale = {1.f, 1.f, 1.f};
};
} // namespace Physics