// #pragma once

// #include "glad/glad.h"
// #include <vector>
// #include <cmath>

// #include "Vec3.hpp"
// #include "Mat4.hpp"

// struct Color
// {
//     union {
//         struct
//         {
//             float r, g, b;
//         };
//         float e[3];
//     };
// };

// enum TYPES
// {
//     NONE = -1,
//     CUBE = 0,
//     SPHERE = 1,
//     CYLINDRE = 2
// };

// class Mesh
// {
// protected:
//     std::vector<Vec3> vertices;
//     short _draw_mode;

// public:
//     Mesh();
//     Mesh(short draw_mode);
//     virtual ~Mesh();

//     virtual void draw(const Mat4 &transform, const Color &color) = 0;
//     virtual void setDrawMode(short draw_mode);
// };

// inline Mesh::Mesh() : _draw_mode{GL_TRIANGLE_STRIP} {}
// inline Mesh::Mesh(short draw_mode) : _draw_mode{draw_mode} {}

// inline Mesh::~Mesh() {}

// inline void Mesh::setDrawMode(short draw_mode)
// {
//     _draw_mode = draw_mode;
// }

// class Cylindre : public Mesh
// {
// private:
// public:
//     Cylindre(unsigned int l);
//     virtual void draw(const Mat4 &transform, const Color &color) override;
// };

// inline Cylindre::Cylindre(unsigned int l) : Mesh{GL_TRIANGLE_STRIP}
// {
//     for (size_t i = 0; i < l + 1; i++)
//     {
//         vertices.push_back({cosf(i * ((float)M_PI * 2) / l), 0, sinf(i * ((float)M_PI * 2) / l)});
//     }
// }

// inline void Cylindre::draw(const Mat4 &transform, const Color &color)
// {
//     glBegin(_draw_mode);

//     glColor3f(color.r, color.g, color.b);
//     for (Vec3 &v : vertices)
//     {

//         const Vec3 t_v{(transform * Vec3{v.x, v.y + 0.5f, v.z}).xyz};
//         glNormal3f(t_v.x, t_v.y, t_v.z);
//         glVertex3f(t_v.x, t_v.y, t_v.z);

//         const Vec3 t_vv{(transform * Vec3{v.x, v.y - 0.5f, v.z}).xyz};
//         glNormal3f(t_vv.x, t_vv.y, t_vv.z);
//         glVertex3f(t_vv.x, t_vv.y, t_vv.z);
//     }

//     glEnd();

//     glBegin(_draw_mode);
//     glColor3f(color.r, color.g, color.b);

//     for (Vec3 &v : vertices)
//     {
//         const Vec3 t_v{(transform * Vec3{v.x, v.y - 0.5f, v.z}).xyz};
//         const Vec3 t_vv{(transform * Vec3{0, v.y - 0.5f, 0}).xyz};
//         glNormal3f(t_vv.x, t_vv.y, t_vv.z);
//         glVertex3f(t_vv.x, t_vv.y, t_vv.z);

//         glNormal3f(t_v.x, t_v.y, t_v.z);
//         glVertex3f(t_v.x, t_v.y, t_v.z);
//     }

//     glEnd();

//     glBegin(_draw_mode);

//     glColor3f(color.r, color.g, color.b);
//     for (Vec3 &v : vertices)
//     {
//         const Vec3 t_v{(transform * Vec3{v.x, v.y + 0.5f, v.z}).xyz};
//         const Vec3 t_vv{(transform * Vec3{0, v.y + 0.5f, 0}).xyz};
//         glNormal3f(t_vv.x, t_vv.y, t_vv.z);
//         glVertex3f(t_vv.x, t_vv.y, t_vv.z);

//         glNormal3f(t_v.x, t_v.y, t_v.z);
//         glVertex3f(t_v.x, t_v.y, t_v.z);
//     }

//     glEnd();

//     glFlush();
// }

// /**
//  * @brief CUBE
//  * 
//  */
// class Cube : public Mesh
// {
// public:
//     Cube();
//     virtual void draw(const Mat4 &transform, const Color &color) override;
// };

// inline Cube::Cube() : Mesh{GL_TRIANGLE_STRIP}
// {
//     vertices.push_back({-0.5, -0.5, 0.5});
//     vertices.push_back({0.5, -0.5, 0.5});
//     vertices.push_back({-0.5, -0.5, -0.5});
//     vertices.push_back({0.5, -0.5, -0.5});
//     vertices.push_back({0.5, 0.5, -0.5});
//     vertices.push_back({0.5, -0.5, 0.5});
//     vertices.push_back({0.5, 0.5, 0.5});
//     vertices.push_back({-0.5, -0.5, 0.5});
//     vertices.push_back({-0.5, 0.5, 0.5});
//     vertices.push_back({-0.5, -0.5, -0.5});
//     vertices.push_back({-0.5, 0.5, -0.5});
//     vertices.push_back({0.5, 0.5, -0.5});
//     vertices.push_back({-0.5, 0.5, 0.5});
//     vertices.push_back({0.5, 0.5, 0.5});
// }

// inline void Cube::draw(const Mat4 &transform, const Color &color)
// {

//     glBegin(_draw_mode);

//     for (Vec3 &v : vertices)
//     {
//         const Vec3 t_v{(transform * v).xyz};
//         glColor3f(color.r, color.g, color.b);
//         glNormal3f(t_v.x, t_v.y, t_v.z);
//         glVertex3f(t_v.x, t_v.y, t_v.z);
//     }

//     glEnd();
//     glFlush();
// }

// /**
//  * @brief SPHERE
//  * 
//  */
// class Sphere : public Mesh
// {
// private:
//     unsigned int m_l;
//     unsigned int m_L;

// public:
//     Sphere(unsigned int l, unsigned int L);
//     virtual void draw(const Mat4 &transform, const Color &color) override;
//     static Vec3 sp(unsigned int i, unsigned int j, unsigned int l, unsigned int L);
// };

// //Sphere
// inline Sphere::Sphere(unsigned int l, unsigned int L) : Mesh{GL_TRIANGLE_STRIP}
// {
//     m_L = L;
//     m_l = l;

//     for (size_t i = 0; i < l; i++)
//         for (size_t j = 0; j < L + 1; j++)
//             vertices.push_back(Sphere::sp(i, j, l, L));

//     vertices.push_back({0, -1, 0});
// }

// inline Vec3 Sphere::sp(unsigned int i, unsigned int j, unsigned int l, unsigned int L)
// {
//     float y = cosf(i * (float)(float)M_PI / l);
//     float r = sinf(i * (float)(float)M_PI / l);
//     float x = cosf(j * ((float)(float)M_PI * 2) / L) * r;
//     float z = sinf(j * ((float)(float)M_PI * 2) / L) * r;

//     return {x, y, z};
// }

// inline void Sphere::draw(const Mat4 &transform, const Color &color)
// {
//     glBegin(_draw_mode);

//     unsigned int i_max = vertices.size() - 1;
//     for (unsigned int i = 0; i < vertices.size(); i++)
//     {
//         int index = i + m_L + 1 > i_max ? i_max : i + m_L + 1;
//         glColor3f(color.r, color.g, color.b);

//         const Vec3 t_v{(transform * vertices[i]).xyz};
//         const Vec3 t_vv{(transform * vertices[index]).xyz};

//         glNormal3f(t_v.x, t_v.y, t_v.z);

//         glVertex3f(t_v.x,
//                    t_v.y,
//                    t_v.z);

//         glNormal3f(t_vv.x, t_vv.y, t_vv.z);

//         glVertex3f(t_vv.x,
//                    t_vv.y,
//                    t_vv.z);
//     }

//     glEnd();
//     glFlush();
// }
