#ifndef __INFINITECYLINDER_HPP__
#define __INFINITECYLINDER_HPP__

#include "Shapes/Shape3D.hpp"
#include "Shapes/Line.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class InfiniteCylinder : public Shape3D
    {
    private:
        Line _line;
        float _radius;

    public:
        InfiniteCylinder(const Line &line, const float radius);
        ~InfiniteCylinder() = default;

        const Line &getLine() const;
        float getRadius() const;

        bool collideWith(const  Point &point, Hit *hit) const ;
        bool collideWith(const  Box &box, Hit *hit) const ;
        bool collideWith(const  Capsule &capsule, Hit *hit) const ;
        bool collideWith(const  Cylinder &cylinder, Hit *hit) const ;
        bool collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const ;
        bool collideWith(const  Line &line, Hit *hit) const ;
        bool collideWith(const  OrientedBox &obox, Hit *hit) const ;
        bool collideWith(const  Plane &plane, Hit *hit) const ;
        bool collideWith(const  Quad &quad, Hit *hit) const ;
        bool collideWith(const  Segment &segment, Hit *hit) const ;
        bool collideWith(const  Sphere &segment, Hit *hit) const ;
    };

    inline const Line &InfiniteCylinder::getLine() const
    {
        return _line;
    }
    inline float InfiniteCylinder::getRadius() const
    {
        return _radius;
    }

    inline InfiniteCylinder::InfiniteCylinder(const Line &line, float radius) : _line{line}, _radius{radius} {}
    inline bool InfiniteCylinder::collideWith(const  Point &point, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Box &box, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Capsule &capsule, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Cylinder &cylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Line &line, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  OrientedBox &obox, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Plane &plane, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Quad &quad, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Segment &segment, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool InfiniteCylinder::collideWith(const  Sphere &segment, Hit *hit) const 
    {
        return false; //todo
    }
} // namespace Core::Maths::Shapes

#endif // __INFINITECYLINDER_HPP__
