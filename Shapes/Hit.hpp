#ifndef __HIT_HPP__
#define __HIT_HPP__

#include "Vec3.hpp"

namespace Core::Maths::Shapes
{
    struct Hit
    {
        Vec3 _point;
        Vec3 _normal;
    };
} // namespace Core::Maths::Shapes


#endif // __HIT_HPP__
