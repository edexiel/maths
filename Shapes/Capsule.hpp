#ifndef __CAPSULE_HPP__
#define __CAPSULE_HPP__

#include "Shapes/Shape3D.hpp"
#include "Shapes/Segment.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class Capsule : public virtual Shape3D
    {
    private:
        Segment _segment;
        float _radius;

    public:
        Capsule() = delete;
        Capsule(const Segment &segment, Hit *hit, const float radius);
        ~Capsule() = delete;

        const Segment &getSegment() const;
        float getRadius() const;

        bool collideWith(const Point &point, Hit *hit) const;
        bool collideWith(const Box &box, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &segment, Hit *hit) const;
    };

    inline Capsule::Capsule(const Segment &segment, Hit *hit, const float radius) : _segment{segment}, _radius{radius} {}

    inline const Segment &Capsule::getSegment() const
    {
        return _segment;
    }
    inline float Capsule::getRadius() const
    {
        return _radius;
    }

    inline bool Capsule::collideWith(const Point &point, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Box &box, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Capsule &capsule, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Plane &plane, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Quad &quad, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Capsule::collideWith(const Sphere &segment, Hit *hit) const
    {
        return false; //todo
    }

} // namespace Core::Maths::Shapes

#endif // __CAPSULE_HPP__
