#ifndef __BOX_HPP__
#define __BOX_HPP__

#include "Shapes/Shape3D.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class Box : public virtual Shape3D
    {
    private:
        Vec3 _origin;
        Vec3 _size;

    public:
        Box(const Vec3 &origin, const Vec3 &size);
        ~Box();

        const Vec3 &getOrigin() const;
        const Vec3 &getSize() const;

        bool collideWith(const Point &point, Hit *hit) const;
        bool collideWith(const Box &box, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &segment, Hit *hit) const;
    };

    inline Box::Box(const Vec3 &origin, const Vec3 &size) : _origin{origin}, _size{size} {}

    inline const Vec3 &Box::getOrigin() const
    {
        return _origin;
    }
    inline const Vec3 &Box::getSize() const
    {
        return _size;
    }

    inline bool Box::collideWith(const Point &point, Hit *hit) const
    {
        return (point.x >= _origin.x - _size.x && point.x <= _origin.x + _size.x) &&
               (point.y >= _origin.y - _size.y && point.y <= _origin.y + _size.y) &&
               (point.z >= _origin.z - _size.z && point.z <= _origin.z + _size.z);
    }

    inline bool Box::collideWith(const Box &box, Hit *hit) const
    {
        return (_origin.x - _size.x <= box._origin.x + box._size.x && _origin.x + _size.x >= box._origin.x - box._size.x) &&
               (_origin.y - _size.z <= box._origin.y + box._size.y && _origin.y + _size.z >= box._origin.y - box._size.y) &&
               (_origin.z - _size.z <= box._origin.z + box._size.z && _origin.z + _size.z >= box._origin.z - box._size.z);
    }
    inline bool Box::collideWith(const Capsule &capsule, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Plane &plane, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Quad &quad, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Box::collideWith(const Sphere &segment, Hit *hit) const
    {
        return false; //todo
    }
} // namespace Core::Maths::Shapes
#endif // __BOX_HPP__