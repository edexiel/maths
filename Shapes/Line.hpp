#ifndef __LINE_HPP__
#define __LINE_HPP__

#include "Shape3D.hpp"
#include "Vec2.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{

    class Line : public Shape3D
    {
    private:
        Vec3 _point;
        Vec3 _direction;

    public:
        Line() = delete;
        Line(const Vec3 &point, const Vec3 &direction);
        Line(const Vec3 &point1, const Vec3 &point2);
        ~Line() = default;

        const Vec3 &getPoint() const;
        const Vec3 &getDirection() const;

        void setPoint(const Vec3 &p);
        void setDirection(const Vec3 &d);

        bool collideWith(const Point &box, Hit *hit) const;
        bool collideWith(const Box &box, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &segment, Hit *hit) const;
    };

    inline Line::Line(const Vec3 &p, const Vec3 &d) : _point{p}, _direction{Vec3::normalize(d)} {}
    inline Line::Line(const Vec3 &p1, const Vec3 &p2) : _point{p1}
    {
        _direction = Vec3::normalize(p2 - p1);
    }

    inline const Vec3 &Line::getPoint() const
    {
        return _point;
    }
    inline const Vec3 &Line::getDirection() const
    {
        return _direction;
    }

    inline void Line::setPoint(const Vec3 &p)
    {
        _point = p;
    }
    inline void Line::setDirection(const Vec3 &d)
    {
        _direction = d;
    }

    inline bool Line::collideWith(const Point &point, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Box &box, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Capsule &capsule, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Plane &plane, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Quad &quad, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Line::collideWith(const Sphere &segment, Hit *hit) const
    {
        return false; //todo
    }

} // namespace Core::Maths::Shapes

#endif // __LINE_HPP__