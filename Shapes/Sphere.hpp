#ifndef __SPHERE_HPP__
#define __SPHERE_HPP__

#include "Shape3D.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class Sphere : public Shape3D
    {
    private:
        Vec3 _origin;
        float _radius;

    public:
        Sphere() = delete;
        Sphere(const Vec3 &p, const float r);

        const Vec3 &getOrigin() const;
        float getRadius() const;

        bool collideWith(const  Point &point, Hit *hit) const ;
        bool collideWith(const  Box &box, Hit *hit) const ;
        bool collideWith(const  Capsule &capsule, Hit *hit) const ;
        bool collideWith(const  Cylinder &cylinder, Hit *hit) const ;
        bool collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const ;
        bool collideWith(const  Line &line, Hit *hit) const ;
        bool collideWith(const  OrientedBox &obox, Hit *hit) const ;
        bool collideWith(const  Plane &plane, Hit *hit) const ;
        bool collideWith(const  Quad &quad, Hit *hit) const ;
        bool collideWith(const  Segment &segment, Hit *hit) const ;
        bool collideWith(const  Sphere &segment, Hit *hit) const ;
    };

    inline Sphere::Sphere(const Vec3 &p, const float r) : _origin{p}, _radius{r} {}

    inline const Vec3 &Sphere::getOrigin() const
    {
        return _origin;
    }
    inline float Sphere::getRadius() const
    {
        return _radius;
    }

    inline bool Sphere::collideWith(const  Point &point, Hit *hit) const 
    {
        if (hit != nullptr)
        {
            hit->_normal = (point - _origin);
            hit->_point = hit->_normal * _radius;
        }

        return (point - _origin).getMagnitude() <= _radius;
    }
    inline bool Sphere::collideWith(const  Box &box, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Capsule &capsule, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Cylinder &cylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Line &line, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  OrientedBox &obox, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Plane &plane, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Quad &quad, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Segment &segment, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Sphere::collideWith(const  Sphere &sphere, Hit *hit) const 
    {
        if (hit != nullptr)
        {
            hit->_normal = (sphere._point - _point);
            hit->_point = hit->_normal * _radius;
        }

        return (sphere._point - _point).getMagnitude() < sphere._radius + _radius;
    }
} // namespace Core::Maths::Shapes

#endif // __SPHERE_HPP__
