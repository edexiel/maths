#ifndef __ORIENTEDBOX_HPP__
#define __ORIENTEDBOX_HPP__

#include "Shapes/Shape3D.hpp"
#include "Referencial.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class OrientedBox : public Shape3D
    {
    private:
        Referencial _referential;
        Vec3 _size;

    public:
        OrientedBox() = delete;
        OrientedBox(const Referencial &referencial, const Vec3 &size);
        ~OrientedBox() = default;

        const Referencial &getReferential() const;
        const Vec3 &getSize() const;

        bool collideWith(const Point &point, Hit *hit) const;
        bool collideWith(const Box &box, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &segment, Hit *hit) const;
    };

    OrientedBox::OrientedBox(const Referencial &referencial, const Vec3 &size) : _referential{referencial}, _size{size} {}

    inline const Referencial &OrientedBox::getReferential() const
    {
        return _referential;
    }
    inline const Vec3 &OrientedBox::getSize() const
    {
        return _size;
    }

    inline bool OrientedBox::collideWith(const Point &point, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Box &box, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Capsule &capsule, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Plane &plane, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Quad &quad, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool OrientedBox::collideWith(const Sphere &segment, Hit *hit) const
    {
        return false; //todo
    }
} // namespace Core::Maths::Shapes

#endif // __ORIENTEDBOX_HPP__
