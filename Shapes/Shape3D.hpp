#ifndef __SHAPE3D_HPP__
#define __SHAPE3D_HPP__

namespace Core::Maths
{
    class Vec3;
}
namespace Core::Maths::Shapes
{

    class Box;
    class Capsule;
    class Cylinder;
    class InfiniteCylinder;
    class Line;
    class OrientedBox;
    class Plane;
    class Quad;
    class Segment;
    class Sphere;

    struct Hit;

    class Shape3D
    {
    public:
        virtual bool collideWith(const Vec3 &point, Hit *hit) const = 0;
        virtual bool collideWith(const Box &box, Hit *hit) const = 0;
        virtual bool collideWith(const Capsule &capsule, Hit *hit) const = 0;
        virtual bool collideWith(const Cylinder &cylinder, Hit *hit) const = 0;
        virtual bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const = 0;
        virtual bool collideWith(const Line &line, Hit *hit) const = 0;
        virtual bool collideWith(const OrientedBox &obox, Hit *hit) const = 0;
        virtual bool collideWith(const Plane &plane, Hit *hit) const = 0;
        virtual bool collideWith(const Quad &quad, Hit *hit) const = 0;
        virtual bool collideWith(const Segment &segment, Hit *hit) const = 0;
        virtual bool collideWith(const Sphere &segment, Hit *hit) const = 0;
    };
} // namespace Core::Maths::Shapes

#endif // __SHAPE3D_HPP__
