#ifndef __PLANE_HPP__
#define __PLANE_HPP__

#include "Shape3D.hpp"
#include "Shapes/Line.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{

    class Plane : public Shape3D
    {
    private:
        Vec3 _normal;
        float _distance;

    public:
        Plane() = delete;
        Plane(const Vec3 &normal, const float &distance);
        Plane(const Vec3 &point1, const Vec3 &point2, const Vec3 &point3);
        Plane(const Vec3 &point, const Vec3 &normal);

        const Vec3 &getNormal() const;
        const float getDistance() const;

        float getSignedDistance(const Vec3 &point);
        Vec3 getClosestPoint(const Vec3 &point);
        bool getSide(const Vec3 &point);
        bool isNearPlane(const Vec3 &point, float maxDistance);

        static bool getIntersect3(const Plane &p1, const Plane &p2, const Plane &p3, Vec3 &result);
        static bool getIntersect2(const Plane &p1, const Plane &p2, Line *result);

        bool collideWith(const Point &point, Hit *hit) const override;
        bool collideWith(const Box &point, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &segment, Hit *hit) const;
    };

    inline Plane::Plane(const Vec3 &normal, const float &distance) : _normal{normal}, _distance{distance} {}

    inline Plane::Plane(const Vec3 &p1, const Vec3 &p2, const Vec3 &p3)
    {
        _normal = Vec3::normalize((p1 - p3) ^ (p1 - p2));
        _distance = Vec3::dot_product(_normal, p1);
    }
    inline Plane::Plane(const Vec3 &point, const Vec3 &normal)
    {
        _normal = Vec3::normalize(normal);
        _distance = Vec3::dot_product(_normal, point);
    }

    inline const Vec3 &Plane::getNormal() const
    {
        return _normal;
    }

    const float Plane::getDistance() const
    {
        return _distance;
    }

    inline float Plane::getSignedDistance(const Vec3 &point)
    {
        return _distance - Vec3::dot_product(point, _normal);
    }
    inline Vec3 Plane::getClosestPoint(const Vec3 &point)
    {
        return point + getSignedDistance(point) * _normal;
    }
    inline bool Plane::getSide(const Vec3 &point)
    {
        return getSignedDistance(point) >= 0;
    }
    inline bool Plane::isNearPlane(const Vec3 &point, float maxDistance)
    {
        return fabsf(getSignedDistance(point) - _distance) <= maxDistance;
    }
    inline bool Plane::getIntersect2(const Plane &p1, const Plane &p2, Line *result)
    {
        if (!result)
            return false;

        Vec3 d = Vec3::cross_product(p1.getNormal(), p2.getNormal());

        float denominateur = Vec3::dot_product(d, d);
        if (denominateur < __FLT_EPSILON__)
            return false;

        Vec3 p = Vec3::cross_product(p1.getDistance() * p2.getNormal() - p2.getDistance() * p1.getNormal(), d) / denominateur;

        result->setDirection(d);
        result->setPoint(p);

        return true;
    }
    inline bool Plane::getIntersect3(const Plane &p1, const Plane &p2, const Plane &p3, Vec3 &result)
    {
        Vec3 u = p2.getNormal() ^ p3.getNormal();
        float denominateur = Vec3::dot_product(p1.getNormal(), u);
        if (fabsf(denominateur) < __FLT_EPSILON__)
            return false;
        Vec3 p = (p1.getDistance() * u + (p1.getNormal() ^ (p3.getDistance() * p2.getNormal() - p2.getDistance() * p3.getNormal())) / denominateur);
        result = p;
        return true;
    }

    inline bool Plane::collideWith(const Point &point, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Box &box, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Capsule &capsule, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Plane &plane, Hit *hit) const
    {
        return getIntersect2(*this, plane, nullptr);
    }
    inline bool Plane::collideWith(const Quad &quad, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Plane::collideWith(const Sphere &segment, Hit *hit) const
    {
        return false; //todo
    }

#endif // __PLANE_HPP__
} // namespace Core::Maths::Shapes