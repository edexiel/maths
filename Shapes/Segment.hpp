#ifndef __SEGMENT_HPP__
#define __SEGMENT_HPP__

#include "Shapes/Shape3D.hpp"
#include "Shapes/Plane.hpp"
#include "Shapes/Sphere.hpp"
#include "Shapes/Quad.hpp"
#include "Shapes/Line.hpp"
#include "Shapes/InfiniteCylinder.hpp"
#include "Shapes/Cylinder.hpp"
#include "Shapes/Capsule.hpp"
#include "Shapes/OrientedBox.hpp"
#include "Shapes/Box.hpp"

#include "Vec3.hpp"
#include "Hit.hpp"

#include "Referencial.hpp"
#include "ToolBox.hpp"

namespace Core::Maths::Shapes
{
    class Segment : public Shape3D
    {
    private:
        Vec3 _p1;
        Vec3 _p2;

    public:
        Segment() = delete;
        Segment(const Vec3 &point1, const Vec3 &Point2);

        const Vec3 &getPoint1() const;
        const Vec3 &getPoint2() const;

        float getSquaredDistance(const Point &point) const;

        bool collideWith(const Point &point, Hit *hit) const;
        bool collideWith(const Box &box, Hit *hit) const;
        bool collideWith(const OrientedBox &obox, Hit *hit) const;
        bool collideWith(const Capsule &capsule, Hit *hit) const;
        bool collideWith(const Cylinder &cylinder, Hit *hit) const;
        bool collideWith(const InfiniteCylinder &icylinder, Hit *hit) const;
        bool collideWith(const Line &line, Hit *hit) const;
        bool collideWith(const Plane &plane, Hit *hit) const;
        bool collideWith(const Quad &quad, Hit *hit) const;
        bool collideWith(const Segment &segment, Hit *hit) const;
        bool collideWith(const Sphere &sphere, Hit *hit) const;
    };

    inline Segment::Segment(const Vec3 &p1, const Vec3 &p2) : _p1{p1}, _p2{p2} {}

    inline const Vec3 &Segment::getPoint1() const
    {
        return _p1;
    }
    inline const Vec3 &Segment::getPoint2() const
    {
        return _p2;
    }

    inline float Segment::getSquaredDistance(const Point &point) const
    {
        Vec3 AM{point - _p1};
        Vec3 AB{_p2 - _p1};

        float t{Vec3::dot_product(AM, AB) / Vec3::dot_product(AB, AB)};

        if (t < 0)
        {
            return Vec3::dot_product(AM, AM);
        }

        if (t > 1)
        {
            return Vec3::dot_product(BM, BM);
        }

        Vec3 result{AM - t * AB};
        return Vec3::dot_product(result, result);
    }

    inline bool Segment::collideWith(const Point &point, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Segment::collideWith(const Box &box, Hit *hit) const
    {

        //La logique ne marche pas, il faudrait pouvoir récupérer t, et choisir le t le plus petit parmi les multiples collisions possible
        // Malheureusement plus de temps de le faire

        Quad px1{};
        Quad px2{};
        Quad py1{};
        Quad py2{};
        Quad pz1{};
        Quad pz2{};

        if (collideWith(px1, hit) || collideWith(px2, hit) || collideWith(py1, hit) || collideWith(py2, hit) || collideWith(pz1, hit) || collideWith(pz2, hit))
        {
            return true;
        }
        return false;
    }
    inline bool Segment::collideWith(const OrientedBox &obox, Hit *hit) const
    {
        Segment s{obox.getReferential().getLocalPosition(_p1), obox.getReferential().getLocalPosition(_p2)};

        Box b{obox.getReferential(), obox.getSize()};

        if (s.collideWith(b, hit))
        {
            hit->_point = obox.getReferential().getGlobalPosition(hit->_point);
            hit->_normal = obox.getReferential().getGlobalPosition(hit->_normal);

            return true;
        }
        return false;
    }
    inline bool Segment::collideWith(const Capsule &capsule, Hit *hit) const
    {
        Cylinder c{capsule.getSegment(), capsule.getRadius()};
        Sphere sp{capsule.getSegment().getPoint1(), capsule.getRadius()};
        Sphere sq{capsule.getSegment().getPoint2(), capsule.getRadius()};

        Hit res;
        if (collideWith(c, &res)) // if it collides with the cylinder
        {
            hit->_normal = res._normal;
            hit->_point = res.point;

            return true;
        }

        if (collideWith(sp, &res)) // if it collides with the sphere in P
        {
            hit->_normal = res._normal;
            hit->_point = res.point;

            return true;
        }

        if (collideWith(sp, &res)) // if it collides with the sphere  in Q
        {
            hit->_normal = res._normal;
            hit->_point = res.point;

            return true;
        }

        return false
    }
    inline bool Segment::collideWith(const Cylinder &cylinder, Hit *hit) const
    {
        Vec3 QP{cylinder.getSegment()._p1 - cylinder.getSegment()._p2};
        Vec3 PQ{cylinder.getSegment()._p2 - cylinder.getSegment()._p1};

        Hit res;
        Line l{cylinder.getSegment()._p1, cylinder.getSegment()._p2 - cylinder.getSegment()._p1};
        InfiniteCylinder ic{l, cylinder.getRadius()};

        if (collideWith(const ic, &res)) // collide with outer shell
        {
            Vec3 QM{res._point - cylinder.getSegment()._p2};

            float proj{Vec3::dot_product(QM, QP)};

            if (proj < 0 || proj > Vec3::dot_product(QP, QP)) // outside range
            {
                return false;
            }

            hit->_point = res._point;
            hit->_normal = res._normal;
            return true;
        }

        //collide with P plan <= R
        Hit resp;
        Plane p{cylinder.getSegment()._p1, QP};
        if (collideWith(const p, &resp))
        {
            if ((cylinder.getSegment()._p1 - resp._point).getMagnitude() <= cylinder.getRadius())
            {
                hit->_point = resp._point;
                hit->_normal = resp._normal;
                return true;
            }
        }

        //collide with Q plan <= R
        Plane q{cylinder.getSegment()._p2, PQ};
        if (collideWith(const q, &resp))
        {
            if ((cylinder.getSegment()._p2 - resp._point).getMagnitude() <= cylinder.getRadius())
            {
                hit->_point = resp._point;
                hit->_normal = resp._normal;
                return true;
            }
        }

        return false;
    }
    inline bool Segment::collideWith(const InfiniteCylinder &icylinder, Hit *hit) const
    {
        // p1 : A
        // p2 : B
        Vec3 AB{_p2 - _p1};
        Vec3 PQ{icylinder.getLine().getDirection() - icylinder.getLine().getPoint()};
        Vec3 PA{_p1 - icylinder.getLine().getPoint()};

        float result;
        Vec3 PQcAB{PQ ^ AB};
        Vec3 PQcPA{PQ ^ PA};

        if (solve(Vec3::dot_product(PQcAB, PQcAB),
                  2 * Vec3::dot_product(PQcPA, PQcAB),
                  Vec3::dot_product(PQcPA, PQcPA) - icylinder.getRadius() * icylinder.getRadius(),
                  &result))
        {
            hit->_point = result * AB;
            float t{Vec3::dot_product(hit->_point - icylinder.getLine().getPoint(), PQ)};
            hit->_normal = (hit->_point - t * PQ) / icylinder.getRadius();

            return true;
        }
        return false;
    }
    inline bool Segment::collideWith(const Line &line, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Segment::collideWith(const Plane &plane, Hit *hit) const
    {
        Vec3 AB = _p2 - _p1;
        float dot = Vec3::dot_product(plane.getNormal(), AB);

        if (dot <= __FLT_MIN__) // Le plan est parallèle avec le vecteur
        {
            //Si le segment n'est pas confondu avec la droite alors return false sinon true
            if (plane.isNearPlane(_p1, __FLT_MIN__))
            {
                hit->_normal = plane.getNormal();
                hit->_point = _p1;
                return true;
            }
            return false;
        }

        if (plane.getSide(_p1) || plane.getSide(_p2))
        {
            float t = (-plane.getDistance() - Vec3::dot_product(_p1, plane.getNormal())) / dot;

            hit->_point = _p1 + AB * t;
            hit->_normal = AB;

            return true;
        }

        return false;
    }
    inline bool Segment::collideWith(const Quad &quad, Hit *hit) const
    {
        const Referencial &ref = quad.getReferencial();
        Vec3 normal = Vec3::normalize(ref.getK());
        float distance = Vec3::dot_product(normal, ref.getOrigin());

        Plane plane{normal, distance};

        Hit h;

        if (collideWith(const plane, &h))
        {
            Vec2 size = quad.getSize();
            Vec3 local = ref.getLocalPosition(h._point);

            if (local.x > -size.x && local.x > size.x &&
                local.y > -size.y && local.y > size.y)
            {
                return true;
            }
        }

        return false;
    }
    inline bool Segment::collideWith(const Segment &segment, Hit *hit) const
    {
        return false; //todo
    }
    inline bool Segment::collideWith(const Sphere &sphere, Hit *hit) const
    {
        // p1 : A
        // p2 : B
        Vec3 AB = _p2 - _p1;
        Vec3 OmegaA = _p1 - sphere.getOrigin();

        float result;

        if (solve(Vec3::dot_product(AB, AB),
                  2 * (Vec3::dot_product(OmegaA, AB)),
                  Vec3::dot_product(AB, AB) - sphere.getRadius() * sphere.getRadius(),
                  &result))
        {
            hit->_point = result * AB;
            hit->_normal = Vec3::normalize(hit->_point);
            return true;
        }
        return false;
    }

} // namespace Core::Maths::Shapes
#endif // __SEGMENT_HPP__