#ifndef __CYLINDER_HPP__
#define __CYLINDER_HPP__

#include "Shapes/Shape3D.hpp"
#include "Shapes/Segment.hpp"
#include "Vec3.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{

    class Cylinder : public Shape3D
    {
    private:
        Segment _segment;
        float _radius;

    public:
        Cylinder() = delete;
        Cylinder(const Segment &segment, const float radius);
        ~Cylinder() = default;

        const Segment &getSegment() const;
        float getRadius() const;

        bool collideWith(const  Point &point, Hit *hit) const ;
        bool collideWith(const  Box &box, Hit *hit) const ;
        bool collideWith(const  Capsule &capsule, Hit *hit) const ;
        bool collideWith(const  Cylinder &cylinder, Hit *hit) const ;
        bool collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const ;
        bool collideWith(const  Line &line, Hit *hit) const ;
        bool collideWith(const  OrientedBox &obox, Hit *hit) const ;
        bool collideWith(const  Plane &plane, Hit *hit) const ;
        bool collideWith(const  Quad &quad, Hit *hit) const ;
        bool collideWith(const  Segment &segment, Hit *hit) const ;
        bool collideWith(const  Sphere &sphere, Hit *hit) const ;
    };

    inline Cylinder::Cylinder(const Segment &segment, const float radius) : _segment{segment}, _radius{radius} {}

    inline const Segment &Cylinder::getSegment() const
    {
        return _segment;
    }
    inline float Cylinder::getRadius() const
    {
        return _radius;
    }

    inline bool Cylinder::collideWith(const  Point &point, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Box &box, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Capsule &capsule, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Cylinder &cylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Line &line, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  OrientedBox &obox, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Plane &plane, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Quad &quad, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Segment &segment, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Cylinder::collideWith(const  Sphere &segment, Hit *hit) const 
    {
        return false; //todo
    }
} // namespace Core::Maths::Shapes

#endif // __CYLINDER_HPP__
