#ifndef __QUAD_HPP__
#define __QUAD_HPP__

#include "Shapes/Shape3D.hpp"
#include "Referencial.hpp"
#include "Vec2.hpp"
#include "Hit.hpp"

namespace Core::Maths::Shapes
{
    class Quad : public Shape3D
    {
    private:
        Referencial _referencial;
        Vec2f _size;

    public:
        Quad() = delete;
        Quad(Referencial referencial, Vec2f size);
        ~Quad() = default;

        const Referencial &getReferencial();
        const Vec2f &getSize();

        bool collideWith(const  Point &point, Hit *hit) const ;
        bool collideWith(const  Box &box, Hit *hit) const ;
        bool collideWith(const  Capsule &capsule, Hit *hit) const ;
        bool collideWith(const  Cylinder &cylinder, Hit *hit) const ;
        bool collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const ;
        bool collideWith(const  Line &line, Hit *hit) const ;
        bool collideWith(const  OrientedBox &obox, Hit *hit) const ;
        bool collideWith(const  Plane &plane, Hit *hit) const ;
        bool collideWith(const  Quad &quad, Hit *hit) const ;
        bool collideWith(const  Segment &segment, Hit *hit) const ;
        bool collideWith(const  Sphere &segment, Hit *hit) const ;
    };

    inline Quad::Quad(Referencial referencial, Vec2f size) : _referencial{referencial}, _size{size} {}

    inline const Referencial &Quad::getReferencial()
    {
        return _referencial;
    }
    inline const Vec2f &Quad::getSize()
    {
        return _size;
    }

    inline bool Quad::collideWith(const  Point &point, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Box &box, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Capsule &capsule, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Cylinder &cylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  InfiniteCylinder &icylinder, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Line &line, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  OrientedBox &obox, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Plane &plane, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Quad &quad, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Segment &segment, Hit *hit) const 
    {
        return false; //todo
    }
    inline bool Quad::collideWith(const  Sphere &segment, Hit *hit) const 
    {
        return false; //todo
    }

} // namespace Core::Maths::Shapes

#endif // __QUAD_HPP__
