#ifndef __REFERENCIAL_HPP__
#define __REFERENCIAL_HPP__

#include "Vec3.hpp"

namespace Core::Maths
{
    class Referencial
    {
    private:
        Vec3 _origin, _I, _J, _K;

    public:
        Referencial(Vec3 origin, Vec3 I, Vec3 J, Vec3 K);
        ~Referencial() = default;

        const Vec3 &getOrigin() const;
        const Vec3 &getI() const;
        const Vec3 &getJ() const;
        const Vec3 &getK() const;

        Vec3 getLocalPosition(const Vec3 &globalPos) const;
        Vec3 getGlobalPosition(const Vec3 &localPos) const;
        Vec3 getLocalVec(const Vec3 &globalVec) const;
        Vec3 getGlobalVec(const Vec3 &localVec) const;
    };

    inline Referencial::Referencial(Vec3 origin, Vec3 I, Vec3 J, Vec3 K) : _origin{origin}, _I{I}, _J{J}, _K{K} {}

    inline const Vec3 &Referencial::getOrigin() const
    {
        return _origin;
    }

    inline const Vec3 &Referencial::getI() const
    { 
        return _I;
    }
    inline const Vec3 &Referencial::getJ() const
    {
        return _J;
    }
    inline const Vec3 &Referencial::getK() const
    {
        return _K;
    }

    inline Vec3 Referencial::getLocalPosition(const Vec3 &globalPos) const
    {

        Vec3 O = globalPos - _origin;

        return Vec3({Vec3::dot_product(O, _I),
                     Vec3::dot_product(O, _J),
                     Vec3::dot_product(O, _K)});
    }
    inline Vec3 Referencial::getGlobalPosition(const Vec3 &localPos) const
    {
        Vec3 local = _I * localPos.x + _J * localPos.y + _K * localPos.z;

        return local + _origin;
    }

    inline Vec3 Referencial::getLocalVec(const Vec3 &globalVec) const
    {
        return Vec3({Vec3::dot_product(globalVec, _I),
                     Vec3::dot_product(globalVec, _J),
                     Vec3::dot_product(globalVec, _K)});
    }
    inline Vec3 Referencial::getGlobalVec(const Vec3 &localVec) const
    {
        return localVec.x * _I + localVec.y * _J + localVec.z * _K;
    }
} // namespace Core::Maths

#endif // __REFERENCIAL_HPP__
