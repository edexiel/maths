#ifndef __CYLINDRICAL_HPP__
#define __CYLINDRICAL_HPP__

#include "Coordinates/Cartesian.hpp"

namespace Core::Maths::Coordinates
{

    float atan2(float a)
    {
        return false; //todo
    }

    class Cylindrical
    {
    public:
        union {
            struct
            {
                float r, theta, z;
            };
            float a[3];
        };
        static Cartesian toCartesian(const Cylindrical &coord);
    };

    inline Cartesian Cylindrical::toCartesian(const Cylindrical &c)
    {
        return {
            c.r * cosf(c.theta),
            c.r * sinf(c.theta),
            c.z};
    }

} // namespace Core::Maths::Coordinates


#endif // __CYLINDRICAL_HPP__
