#ifndef __CARTESIAN_HPP__
#define __CARTESIAN_HPP__

#include <cmath>
#include "Coordinates/Spherical.hpp"
#include "Coordinates/Cylindrical.hpp"

namespace Core::Maths::Coordinates
{
    class Cartesian
    {
    public:
        union {
            struct
            {
                float x, y, z;
            };
            float e[3];
        };
        static Cylindrical toCylindrical(const Cartesian &coord);
        static Spherical toSpherical(const Cartesian &coord);
    };

    inline Cylindrical Cartesian::toCylindrical(const Cartesian &coord)
    {
        return false; //todo
    }

    inline Spherical Cartesian::toSpherical(const Cartesian &c)
    {
        const float r = sqrtf(c.x * c.x + c.y * c.y + c.z * c.z);
        return
        {
            r,
                Maths::Coordinates::atan2(),
                acosf(c.z / r)
        }
    }

} // namespace Core::Maths::Coordinates


#endif // __CARTESIAN_HPP__
