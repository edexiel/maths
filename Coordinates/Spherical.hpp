#pragma once

#include "Cartesian.hpp"

namespace Core::Maths::Coordinates
{
    class Spherical
    {
    public:
        union {
            struct
            {
                float r, theta, phi;
            };
            float a[3];
        };

        static Cartesian toCartesian(const Spherical &coord);
    };

    inline Cartesian Spherical::toCartesian(const Spherical &c)
    {
        const float y = c.r * sinf(c.phi) * sinf(c.theta);

        return {
            c.r * sinf(c.phi) * cosf(c.theta),
            y,
            c.r * cosf(y)};
    }

} // namespace Core::Maths::Coordinates