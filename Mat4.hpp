#ifndef __MAT4_HPP__
#define __MAT4_HPP__

#include <cmath>
#include "Vec4.hpp"
#include "Transform.hpp"

namespace Core::Maths
{
    class Mat4
    {
    public:
        union
        {
            Vec4 v[4];
            float a[16];
            float aa[4][4];
        };

        Mat4() = default;
        Mat4(float f);
        Mat4(const float (&array)[16]);
        Mat4(const Vec4 &v1, const Vec4 &v2, const Vec4 &v3, const Vec4 &v4);

        static Mat4 identity();
        static Mat4 viewportMatrix(int x, int y, int width, int height);
        static Mat4 orthoMatrix(float left, float right, float bottom, float top, float near, float far);
        static Mat4 frustum(float left, float right, float bottom, float top, float znear, float zfar);
        static Mat4 perspective(float fovyInDegrees, float aspectRatio, float znear, float zfar);
        static Mat4 CreateTranslationMatrix(const Vec3 &translation);
        static Mat4 CreateScaleMatrix(const Vec3 &scale);
        static Mat4 CreateXRotationMatrix(const float angle);
        static Mat4 CreateYRotationMatrix(const float angle);
        static Mat4 CreateZRotationMatrix(const float angle);
        static Mat4 CreateRotationMatrix(const Vec3 &rotation);
        static Mat4 CreateAxisRotationMatrix(const float angle, const Vec3 &axis);
        static Mat4 CreateTransformMatrix(const Vec3 &position, const Vec3 &rotation, const Vec3 &scale);
        static Mat4 CreateTransformMatrix(const Physics::Transform &transform);

        static Mat4 transpose(const Mat4 &mat);
        static Mat4 adjugate(const Mat4 &mat);
        static Mat4 invert(const Mat4 &mat);

        float determinant() const;
        float cofactor() const;
        Mat4 comatrix() const;
        void transpose();
        void invert();
        Vec4 solve();

        bool operator==(const Mat4 &other) const;
        Mat4 operator+(const Mat4 &other) const;
        Mat4 operator-(const Mat4 &other) const;
        Mat4 operator*(const Mat4 &other) const;
        Vec4 operator*(const Vec4 &v) const;
    };

    inline Mat4::Mat4(float f) : a{f} {}
    inline Mat4::Mat4(const Vec4 &v1, const Vec4 &v2, const Vec4 &v3, const Vec4 &v4)
    {
        v[0] = v1;
        v[1] = v2;
        v[2] = v3;
        v[3] = v4;
    }
    inline Mat4::Mat4(const float (&array)[16]) : a{*array} {}

    inline Mat4 Mat4::identity()
    {
        return Mat4{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
    }

    /**
 * @brief 
 * 
 * @param x largeur du clip space
 * @param y hauteur du clip space
 * @param width  largeur de la viewport
 * @param height hauteur de la viewport
 * 
 * @return Mat4 
 */
    inline Mat4 Mat4::viewportMatrix(int x, int y, int width, int height)
    {
        return {{(float)width / ((float)x * 2.f), 0.f, 0.f, 0.f}, {0.f, -((float)height / ((float)y * 2.f)), 0.f, 0.f}, {0.f, 0.f, 1.f, 0.f}, {(float)width * 0.5f, (float)height * 0.5f, 0.f, 1.f}};
    }

    /**
 * @brief Creates orthographic matric (2D)
 * 
 * @param left 
 * @param right 
 * @param bottom 
 * @param top 
 * @param near 
 * @param far 
 * 
 * @return Mat4 
 */
    inline Mat4 Mat4::orthoMatrix(float left, float right, float bottom, float top, float near, float far)
    {
        return {{2.f / (right - left), 0.f, 0.f, 0.f}, {0.f, 2.f / (top - bottom), 0.f, 0.f}, {0.f, 0.f, -2.f / (far - near), 0.f}, {-((right + left) / (right - left)), -((top + bottom) / (top - bottom)), -((far + near) / (far - near)), 1.f}};
    }

    /**
 * @brief Creates perspective matrix (3D)
 * 
 * @param fovyInDegrees 
 * @param aspectRatio 
 * @param znear 
 * @param zfar 
 * @return Mat4 
 */
    inline Mat4 Mat4::perspective(float fovyInDegrees, float aspectRatio, float znear, float zfar)
    {
        float ymax, xmax;
        ymax = znear * tanf(fovyInDegrees * (float)(float)M_PI / 360.0f);
        // ymin = -ymax;
        // xmin = -ymax * aspectRatio;
        xmax = ymax * aspectRatio;
        return frustum(-xmax, xmax, -ymax, ymax, znear, zfar);
    }

    /**
 * @brief Creates a frustum
 * 
 * @param left 
 * @param right 
 * @param bottom 
 * @param top 
 * @param znear 
 * @param zfar 
 * @return Mat4 
 */
    inline Mat4 Mat4::frustum(float left, float right, float bottom, float top, float znear, float zfar)
    {
        float nn, rl, tb, fn;
        nn = 2.0f * znear;
        rl = right - left;
        tb = top - bottom;
        fn = zfar - znear;

        return {{nn / rl, 0.f, 0.f, 0.f}, {0.f, nn / tb, 0.f, 0.f}, {(right + left) / rl, (top + bottom) / tb, -(zfar + znear) / fn, -1.0}, {0, 0, -(nn * zfar) / fn, 0.f}};
    }

    inline Mat4 Mat4::CreateTranslationMatrix(const Vec3 &t)
    {
        return Mat4{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {t.x, t.y, t.z, 1}};
    }

    inline Mat4 Mat4::CreateScaleMatrix(const Vec3 &scale)
    {
        return Mat4{{scale.x, 0, 0, 0}, {0, scale.y, 0, 0}, {0, 0, scale.z, 0}, {0, 0, 0, 1}};
    }

    /**
 * @brief Creates a rotation matrix on X axis
 * 
 * @param angle in radians
 * 
 * @return Mat4 
 */
    inline Mat4 Mat4::CreateXRotationMatrix(const float angle)
    {
        float c, s;
        sincosf(angle, &s, &c);

        return Mat4{{1, 0, 0, 0}, {0, c, s, 0}, {0, -s, c, 0}, {0, 0, 0, 1}};
    }

    /**
 * @brief Creates a rotation matrix on Y axis
 * 
 * @param angle in radians
 * 
 * @return Mat4 
 */
    inline Mat4 Mat4::CreateYRotationMatrix(const float angle)
    {
        float c, s;
        sincosf(angle, &s, &c);

        return Mat4{{c, 0, -s, 0}, {0, 1, 0, 0}, {s, 0, c, 0}, {0, 0, 0, 1}};
    }

    /**
 * @brief Creates a rotation matrix on Z axis
 * 
 * @param angle in radians
 * 
 * @return Mat4 
 */
    inline Mat4 Mat4::CreateZRotationMatrix(const float angle)
    {
        float c, s;
        sincosf(angle, &s, &c);

        return Mat4{{c, s, 0, 0}, {-s, c, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
    }

    inline Mat4 Mat4::CreateRotationMatrix(const Vec3 &rotation)
    {
        return CreateYRotationMatrix(rotation.y) * CreateXRotationMatrix(rotation.x) * CreateZRotationMatrix(rotation.z);
    }

    inline Mat4 Mat4::CreateAxisRotationMatrix(const float angle, const Vec3 &axis)
    {
        float tsin, tcos;
        sincosf(angle, &tsin, &tcos);

        return Mat4{{(axis.x * axis.x + (1 - tcos) + tcos), axis.x * axis.y * (1 - tcos) + axis.z * tsin, axis.x * axis.z * (1 - tcos) - axis.y * tsin, 0},
                    {axis.x * axis.y * (1 - tcos) - axis.z * tsin, axis.y * axis.y + (1 - tcos) + tcos, axis.y * axis.z * (1 - tcos) + axis.x * tsin, 0},
                    {axis.x * axis.z * (1 - tcos) + axis.y * tsin, axis.y * axis.z + (1 - tcos) - axis.x * tsin, axis.z * axis.z * (1 - tcos) + tcos, 0},
                    {0, 0, 0, 1}};
    }

    inline Mat4 Mat4::CreateTransformMatrix(const Vec3 &position, const Vec3 &rotation, const Vec3 &scale)
    {
        return CreateTranslationMatrix(position) *
               (CreateYRotationMatrix(rotation.y) * CreateXRotationMatrix(rotation.x) * CreateZRotationMatrix(rotation.z)) *
               CreateScaleMatrix(scale);
    }

    inline Mat4 Mat4::CreateTransformMatrix(const Physics::Transform &transform)
    {
        return CreateTranslationMatrix(transform.position.xyz) *
               (CreateYRotationMatrix(transform.rotation.y) * CreateXRotationMatrix(transform.rotation.x) * CreateZRotationMatrix(transform.rotation.z)) *
               CreateScaleMatrix(transform.scale);
    }

    // inline Mat4 Mat4::transpose(const Mat4 &mat)
    // {
    //     //todo
    // }
    // inline Mat4 Mat4::adjugate(const Mat4 &mat)
    // {
    //     //todo
    // }
    // inline Mat4 Mat4::invert(const Mat4 &mat)
    // {
    //     //todo
    // }

    // inline float Mat4::determinant() const
    // {
    //     //todo
    // }
    // inline float Mat4::cofactor() const
    // {
    //     //todo
    // }
    // inline Mat4 Mat4::comatrix() const
    // {
    //     //todo
    // }
    // inline void Mat4::transpose()
    // {
    //     //todo
    // }
    // inline void Mat4::invert()
    // {
    // }

    // inline Vec4 Mat4::solve()
    // {
    // }

    inline Mat4 Mat4::operator+(const Mat4 &other) const
    {
        Mat4 res{0};

        for (int i = 0; i < 16; i++)
            res.a[i] = a[i] + other.a[i];

        return res;
    }
    inline Mat4 Mat4::operator-(const Mat4 &other) const
    {
        Mat4 res{0};

        for (int i = 0; i < 16; i++)
            res.a[i] = a[i] - other.a[i];

        return res;
    }

    inline Mat4 Mat4::operator*(const Mat4 &other) const
    {
        Mat4 res = {0};

        for (int c = 0; c < 4; ++c)
            for (int r = 0; r < 4; ++r)
                for (int i = 0; i < 4; ++i)
                    res.v[c].e[r] += v[i].e[r] * other.v[c].e[i];
        return res;
    }

    inline Vec4 Mat4::operator*(const Vec4 &_v) const
    {
        Vec4 res{0, 0, 0, 0};

        res.x = _v.x * aa[0][0] + _v.y * aa[1][0] + _v.z * aa[2][0] + _v.w * aa[3][0];
        res.y = _v.x * aa[0][1] + _v.y * aa[1][1] + _v.z * aa[2][1] + _v.w * aa[3][1];
        res.z = _v.x * aa[0][2] + _v.y * aa[1][2] + _v.z * aa[2][2] + _v.w * aa[3][2];
        res.w = _v.x * aa[0][3] + _v.y * aa[1][3] + _v.z * aa[2][3] + _v.w * aa[3][3];

        return res;
    }
} // namespace Core::Maths

#endif // __MAT4_HPP__
